# About this project

This project is called **Pocket Frogs Word Generator** (or simply `pocket-frogs-wordgen`) made for [Pocket Frogs Discord server](https://discord.gg/XZ3eeEp).

It is made up to help hosting frog giveaways to be easier. Sometimes a giveaway has large roster of frogs, and identifying each frog can be hard. Using frog codes, the program will generate frog names, making it easier to know your frog roster before hosting a giveaway.

An explaination of frog code is shown here:
![](https://cdn.discordapp.com/attachments/684809246826823761/813570115475996672/image0.jpg)

## Setting up the workspace

### Requirements

-   Node.js (v16+)
-   Yarn
-   Git

1. Clone this repository.

```bash
git clone git@gitlab.com:KiattipoomR/pocket-frogs-wordgen.git
```

2. Navigate to the folder and install dependencies.

```bash
yarn
```

## Usage

1. Create a file called `in.csv`. We will use that file to be an input for the program.
2. Prepare data to be fed. The data in each line should be in the following format:

```
{Habitat number},{Frog codes}

Example:
1,1:18:11:21,1:4:11:21,1:9:4:21,1:3:4:21,1:13:5:21,1:10:5:21
```

For the frog codes, you can obtain by clicking the gears icon in "Habitat Options" of each habitat. The code will be automatically copied to the clipboard.

![](https://media.discordapp.net/attachments/684809246826823761/684848526962065470/image0.png)

3. Run the program with the command:

```bash
yarn start
```

The console should output the result in the following format:

```
{Primary color} {Secondary color} {Breed} ({Habitat number}-{Frog number}) / ... (depends on the number of frogs in the input)

Example result using the input "1,1:18:11:21,1:4:11:21,1:9:4:21,1:3:4:21,1:13:5:21,1:10:5:21":
Maroon Tingo Sagitta (1-1) / Red Tingo Sagitta (1-2) / Tangelo Carota Sagitta (1-3) / Orange Carota Sagitta (1-4) / Gold Aurum Sagitta (1-5) / Yellow Aurum Sagitta (1-6)
```

After that, you can copy the result and post in the `#gift_offers_and_requests` channel along with the screenshot of your frog roster (if you have the permission to post the photo) when hosting a giveaway. :D
