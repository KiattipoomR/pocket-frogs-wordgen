import { Breed } from "./src/consts/frog-breed";
import { PrimaryColor, SecondaryColor } from "./src/consts/frog-color";
import * as fs from "fs";

const convertCodeToName = (code: string): string => {
	const [, primaryColor, secondaryColor, breed] = code.split(":");
	return `${PrimaryColor[primaryColor]} ${SecondaryColor[secondaryColor]} ${Breed[breed]}`;
};

const rows = fs.readFileSync("./in.csv").toString().split(/\r?\n/);
rows.forEach((row) => {
	const [habitat, ...frogs] = row.split(",");
	const convertedFrogNames = frogs.map(
		(frog, index) => `${convertCodeToName(frog)} (${habitat}-${index + 1})`,
	);

	console.log(convertedFrogNames.join(" / "));
});
